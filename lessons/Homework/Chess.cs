﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lessons
{
    public class Chess
    {

        public char[,] GameDesk = new char[8, 8];
        public char[,] DisplayDesk = new char[18, 18];

        public void Play()
        {
            System.Console.OutputEncoding = System.Text.Encoding.UTF8;

            Console.Clear();

            DeeU.Print("Press Enter to start game", "green");
            bool is_started = false;
            while (!is_started)
            {

                var input = Console.ReadKey();

                if (input.Key == ConsoleKey.Escape)
                    break;
                else if (input.Key == ConsoleKey.Enter)
                {
                    is_started = true;

                    Console.WindowWidth = 40;
                    Console.WindowHeight = 20;

                    FillGameDesk();
                    PrintP();
                }
                else
                {
                    Console.Clear();
                    DeeU.Print("Press Enter to start game", "green");
                }
            }



            //PrintG();
        }



        void FillGameDesk()
        {

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    GameDesk[1, i] = '♟';
                    GameDesk[6, i] = '♙';
                }

            }

            GameDesk[0, 0] = '♜'; GameDesk[7, 0] = '♖'; GameDesk[0, 7] = '♜'; GameDesk[7, 7] = '♖';
            GameDesk[7, 6] = '♘'; GameDesk[0, 6] = '♞'; GameDesk[7, 1] = '♘'; GameDesk[0, 1] = '♞';
            GameDesk[7, 2] = '♗'; GameDesk[0, 2] = '♝'; GameDesk[7, 5] = '♗'; GameDesk[0, 5] = '♝';
            GameDesk[7, 4] = '♕'; GameDesk[0, 4] = '♛';
            GameDesk[7, 3] = '♔'; GameDesk[0, 3] = '♚';


        }
        void PrintP()
        {
            FillDisplayDesk();
            for (int i = 0; i < 17; i++)
            {
                for (int j = 0; j < 17; j++)
                {
                    Console.Write(DisplayDesk[i, j]);
                    Console.Write(" ");
                }
                Console.Write("\n");
            }
        }
        void PrintG()
        {
            FillDisplayDesk();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Console.Write(GameDesk[i, j]);
                    Console.Write(" ");
                }
                Console.Write("\n");
            }
        }
        public void FillDisplayDesk()
        {
            Console.Clear();
            string letters = "ABCDEFGH";
            int index = 0;
            int row = 0;
            int col = 0;


            for (int i = 2; i < 17; i += 2)
            {
                // Fill characters in DisplayDesk
                for (int j = 2; j < 17; j += 2)
                {
                    DisplayDesk[i, j] = GameDesk[row, col];

                    col++;
                    if (col == 8)
                        col = 0;
                }

                if (row == 7)
                    row = 0;
                row++;

                // Fill letters in DisplayDesk 
                DisplayDesk[0, i] = letters[index];
                if (index + 1 < letters.Length)
                    index++;
                // Fill numbers in DisplayDesk 
                DisplayDesk[i, 0] = index.ToString()[0];

            }
            DisplayDesk[16, 0] = '8';

        }

    }


    public class Notify
    {
        
    }



}
